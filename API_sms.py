import base64
import datetime
from pprint import pprint
import re
import requests
import xml.etree.ElementTree as ET


# This script is a library to send and receive sms using a huawei dongle through its API.
# The final purpose is to connect it to a website to send reminder sms.
# It's under development
# Features to add:
# - Test if the dongle is in network mode and not in Mass Storage Volume
# - Collect the data on the web site (number of the participant, name of the experimenter, hours of the test) and parse them 
# - Make a cron task to execute the sript once a day at a given time.
# - Turn it to Mass Volume Storage when the script's job is done


# If the Dongle is detected as USB "Mass storage mode" run this
# sudo usb_modeswitch -W -v 0x12d1 -p 0x1f01 -M "55534243123456780000000000000a11062000000000000100000000000000" -R


SMS_LIST_TEMPLATE = '<request>' +\
	'<PageIndex>1</PageIndex>' +\
	'<ReadCount>20</ReadCount>' +\
	'<BoxType>2</BoxType>' +\
	'<SortType>0</SortType>' +\
	'<Ascending>0</Ascending>' +\
	'<UnreadPreferred>0</UnreadPreferred>' +\
	'</request>'


SMS_SEND_TEMPLATE = '<request>' +\
    '<Index>-1</Index>' +\
    '<Phones><Phone>{phone}</Phone></Phones>' +\
    '<Sca/>' +\
    '<Content>{content}</Content>' +\
    '<Length>{length}</Length>' +\
    '<Reserved>1</Reserved>' +\
    '<Date>{timestamp}</Date>' +\
	'</request>'


sms_gateway_ip = 'http://192.168.8.1'




def GetSmsList(sms_gateway_ip):
		class SMS:
		    Opened = False
		    Message = ''
		
		resultsList = list()

		url = '/api/sms/sms-list'
		token,sessionID = GetTokenAndSessionID(sms_gateway_ip)
		headers	= {	'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
					'__RequestVerificationToken': token,
					'Cookie': sessionID
				  }
		r = requests.post(sms_gateway_ip + url, data = SMS_LIST_TEMPLATE, headers=headers)
			
		root = ET.fromstring(r.text)


		for messages in root.iter('Messages'):
			for message in messages :
				sms = SMS()
				sms.Message = message.find('Content').text
				sms.Opened = False if message.find('SmsType').text == '1' else True
				resultsList.append(sms)


	print(resultsList[1].Message)
	

def GetTokenAndSessionID(sms_gateway_ip):
	url = '/html/smsinbox.html'
	r = requests.get(sms_gateway_ip + url)
	Setcookie = r.headers.get('set-cookie')
	sessionID = Setcookie.split(';')[0]
	token =  re.findall(r'"([^"]*)"', r.text)[2]
	return token, sessionID
	


def SendSMS(phone, content):
	url = '/api/sms/send-sms'
	timestamp = datetime.datetime.now() + datetime.timedelta(seconds=15)
	token,sessionID = GetTokenAndSessionID(sms_gateway_ip)
	print(token)

	headers	= {	'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
				'__RequestVerificationToken': token,
				'X-Requested-With': 'XMLHttpRequest',
				'Cookie': sessionID
			  }
	print(headers)
	data = SMS_SEND_TEMPLATE.format(phone=phone, content=content, length=len(content), timestamp=str(timestamp.strftime("%Y-%m-%d %H:%M:%S")))
	print(data)
	r1 = requests.post(sms_gateway_ip + url, data=data, headers=headers)
	
	print("r1.text :" + r1.text)

	



#SendSMS('+33688712667', 'Ceci est un test avant demain matin. Message en provenance de ma clé huawei!')
GetSmsList(sms_gateway_ip)




